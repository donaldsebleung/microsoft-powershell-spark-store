PowerShell Core 是微软自 6.x 版本从 Windows PowerShell fork 出来的跨平台 shell。该 shell 结合了 Unix shell 的弹性及主流编程语言的抽象化，让您能写出更易读，耐用的脚本。跨平台 PowerShell 最适合常用微软 Azure 云服务的 DevOps 人员及从 Windows 世界迁移过来但没时间摸透 Linux 的系统管理人员。

声明：本软件包并非由微软官方提供或支持，如有问题请发邮件到 donaldsebleung@qq.com 或于 https://gitee.com/donaldsebleung/microsoft-powershell-spark-store 建工单。
