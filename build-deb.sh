#!/bin/bash -e

rm -rf ./dl-assets/
mkdir -p ./dl-assets/
pushd ./dl-assets/
wget https://github.com/PowerShell/PowerShell/releases/download/v7.3.4/powershell-7.3.4-linux-x64.tar.gz
tar xvf powershell-7.3.4-linux-x64.tar.gz
rm -vf powershell-7.3.4-linux-x64.tar.gz
chmod +x ./pwsh
popd
dpkg-buildpackage -us -uc
lintian ../microsoft-powershell_7.3.4~spark1_amd64.deb
